﻿using Newtonsoft.Json;

namespace Taskerino
{
    public enum Priority
    {
        Low,
        Medium,
        High
    }

    public class Task
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Priority Priority { get; set; }
        public DateTime DueDate { get; set; }
        public Stack<Task> History { get; set; }

        public Task(string name, string description, Priority priority, DateTime dueDate)
        {
            Name = name;
            Description = description;
            Priority = priority;
            DueDate = dueDate;
            History = new Stack<Task>();
        }

        public void SetName(string taskName)
        {
            Name = taskName;
        }

        public void UndoLastChange()
        {
            if (History.Count == 0)
            {
                Console.WriteLine("No history to undo");
                return;
            }

            Task lastEntry = History.Peek();

            this.Name = lastEntry.Name;
            this.Description = lastEntry.Description;
            this.Priority = lastEntry.Priority;
            this.DueDate = lastEntry.DueDate;
        }

        public void PushToHistory()
        {
            History.Push(new Task(this.Name, this.Description, this.Priority, this.DueDate));
        }

        public void PopFromHistory()
        {
            if (History.Count == 0)
            {
                Console.WriteLine("No history to pop");
                return;
            }

            History.Pop();
        }

        public override string ToString()
        {
            return $"Task: {Name}, Priority: {Priority}, Due Date: {DueDate}";
        }
    }

    public class Node<T>
    {
        public T Data { get; set; }
        public Node<T> Next { get; set; }

        public Node(T data)
        {
            Data = data;
            Next = null;
        }
    }

    public class LinkedList<T>
    {
        private Node<T> head;

        public LinkedList()
        {
            head = null;
        }

        public void SetHead(Node<T> newHead)
        {
            head = newHead;
        }

        public Node<T> GetHead()
        {
            return head;
        }

        public void PrintAll(Node<T> current)
        {
            if (current == null)
            {
                return;
            }

            Console.WriteLine(current.Data);
            PrintAll(current.Next);
        }

        public void Add(T data)
        {
            Node<T> newNode = new Node<T>(data);
            if (head == null)
            {
                head = newNode;
            }
            else
            {
                Node<T> current = head;
                while (current.Next != null)
                {
                    current = current.Next;
                }
                current.Next = newNode;
            }
        }

        public void Remove(Node<T> nodeToRemove)
        {
            if (head == null)
                return;

            if (head == nodeToRemove)
            {
                head = head.Next;
                return;
            }

            Node<T> current = head;
            while (current.Next != null)
            {
                if (current.Next == nodeToRemove)
                {
                    current.Next = current.Next.Next;
                    return;
                }
                current = current.Next;
            }
        }

        public void Remove(params Node<T>[] nodesToRemove)
        {
            foreach (var node in nodesToRemove)
            {
                Remove(node);
            }
        }

        public Node<T> FindNodeByData(T data)
        {
            Node<T> current = head;
            while (current != null)
            {
                if (current.Data.Equals(data))
                {
                    return current;
                }
                current = current.Next;
            }
            return null;
        }

        public void Print()
        {
            Node<T> current = head;
            while (current != null)
            {
                Console.Write(current.Data + " ");
                current = current.Next;
            }
            Console.WriteLine();
        }
    }

    public class Program
    {
        static void SerializeToJson(LinkedList<Task> taskList, string filePath)
        {
            List<Task> serializedTasks = new List<Task>();

            var current = taskList.GetHead();
            while (current != null)
            {
                serializedTasks.Add(current.Data);
                current = current.Next;
            }

            string json = JsonConvert.SerializeObject(serializedTasks, Formatting.Indented);
            File.WriteAllText(filePath, json);
            Console.WriteLine($"Task list serialized to {filePath}");
        }

        static LinkedList<Task> DeserializeFromJson(string filePath)
        {
            string json = File.ReadAllText(filePath);
            List<Task> deserializedTaskList = JsonConvert.DeserializeObject<List<Task>>(json);

            LinkedList<Task> linkedList = new LinkedList<Task>();
            foreach (var task in deserializedTaskList)
            {
                linkedList.Add(task);
            }

            return linkedList;
        }

        static void AddTask(LinkedList<Task> taskList)
        {
            Console.WriteLine("Add Task");
            Console.Write("Enter task name: ");
            string name = Console.ReadLine();
            Console.Write("Enter task description: ");
            string description = Console.ReadLine();
            Console.Write("Enter task priority (Low, Medium, High): ");
            Priority priority;
            if (!Enum.TryParse(Console.ReadLine(), out priority))
            {
                Console.WriteLine("Invalid priority. Task not added.");
                return;
            }
            Console.Write("Enter task due date (YYYY-MM-DD): ");
            DateTime dueDate;
            if (!DateTime.TryParse(Console.ReadLine(), out dueDate))
            {
                Console.WriteLine("Invalid due date format. Task not added.");
                return;
            }

            Task newTask = new Task(name, description, priority, dueDate);
            taskList.Add(newTask);

            Console.WriteLine("Task added successfully.");
        }

        static void RemoveTask(LinkedList<Task> taskList)
        {
            Console.WriteLine("Remove Task");

            int index = 1;
            var current = taskList.GetHead();
            while (current != null)
            {
                Console.WriteLine($"{index}. {current.Data}");
                current = current.Next;
                index++;
            }

            Console.Write("Enter the number of the task to remove: ");
            if (!int.TryParse(Console.ReadLine(), out int taskNumber) || taskNumber < 1 || taskNumber >= index)
            {
                Console.WriteLine("Invalid task number. Task not removed.");
                return;
            }

            current = taskList.GetHead();
            for (int i = 1; i < taskNumber - 1; i++)
            {
                current = current.Next;
            }

            taskList.Remove(current.Next);

            Console.WriteLine("Task removed successfully.");
        }

        static void EditTask(LinkedList<Task> taskList)
        {
            Console.WriteLine("Edit Task");

            int index = 1;
            var current = taskList.GetHead();
            while (current != null)
            {
                Console.WriteLine($"{index}. {current.Data}");
                current = current.Next;
                index++;
            }

            Console.Write("Enter the number of the task to edit: ");
            if (!int.TryParse(Console.ReadLine(), out int taskNumber) || taskNumber < 1 || taskNumber >= index)
            {
                Console.WriteLine("Invalid task number. Task not edited.");
                return;
            }

            current = taskList.GetHead();
            for (int i = 1; i < taskNumber; i++)
            {
                current = current.Next;
            }

            Console.WriteLine("Enter new task details:");
            Console.Write("Name: ");
            string name = Console.ReadLine();
            Console.Write("Description: ");
            string description = Console.ReadLine();
            Console.Write("Priority (Low, Medium, High): ");
            Priority priority;
            if (!Enum.TryParse(Console.ReadLine(), out priority))
            {
                Console.WriteLine("Invalid priority. Task not edited.");
                return;
            }
            Console.Write("Due Date (YYYY-MM-DD): ");
            DateTime dueDate;
            if (!DateTime.TryParse(Console.ReadLine(), out dueDate))
            {
                Console.WriteLine("Invalid due date format. Task not edited.");
                return;
            }

            current.Data.SetName(name);
            current.Data.Description = description;
            current.Data.Priority = priority;
            current.Data.DueDate = dueDate;

            current.Data.PushToHistory();

            Console.WriteLine("Task edited successfully.");
        }

        static void ViewTaskHistory(LinkedList<Task> taskList)
        {
            Console.WriteLine("View Task History");

            int index = 1;
            var current = taskList.GetHead();
            while (current != null)
            {
                Console.WriteLine($"{index}. {current.Data}");
                current = current.Next;
                index++;
            }

            Console.Write("Enter the number of the task to view history: ");
            if (!int.TryParse(Console.ReadLine(), out int taskNumber) || taskNumber < 1 || taskNumber >= index)
            {
                Console.WriteLine("Invalid task number. Task history not viewed.");
                return;
            }

            current = taskList.GetHead();
            for (int i = 1; i < taskNumber; i++)
            {
                current = current.Next;
            }

            Console.WriteLine("Task History:");
            Stack<Task> history = current.Data.History;
            int historyIndex = 1;
            foreach (Task task in history)
            {
                Console.WriteLine($"{historyIndex}. {task}");
                historyIndex++;
            }

            Console.Write("Enter the number of the history entry to roll back to (0 to cancel): ");
            if (!int.TryParse(Console.ReadLine(), out int historyNumber) || historyNumber < 1 || historyNumber > history.Count)
            {
                Console.WriteLine("Invalid history entry number. Task not rolled back.");
                return;
            }

            if (historyNumber == 0)
            {
                Console.WriteLine("Task rollback canceled.");
            }
            else
            {
                Task selectedHistory = null;
                for (int i = 1; i <= history.Count; i++)
                {
                    selectedHistory = history.Pop();
                    if (i == historyNumber)
                    {
                        break;
                    }
                }

                current.Data.SetName(selectedHistory.Name);
                current.Data.Description = selectedHistory.Description;
                current.Data.Priority = selectedHistory.Priority;
                current.Data.DueDate = selectedHistory.DueDate;

                Console.WriteLine("Task rolled back successfully.");
            }
        }

        static void Main(string[] args)
        {
            Console.Clear();

            string jsonFilePath = "taskList.json";
            LinkedList<Task> taskList;

            if (File.Exists(jsonFilePath))
            {
                taskList = DeserializeFromJson(jsonFilePath);
            }
            else
            {
                taskList = new LinkedList<Task>();
            }

            bool exit = false;
            while (!exit)
            {
                Console.WriteLine("Task Manager");
                Console.WriteLine("1. Add Task");
                Console.WriteLine("2. Remove Task");
                Console.WriteLine("3. Edit Task");
                Console.WriteLine("4. View All Tasks");
                Console.WriteLine("5. View Task History");
                Console.WriteLine("6. Save and exit");
                Console.WriteLine("6. Exit without saving");
                Console.Write("Choose an option: ");

                string input = Console.ReadLine();

                switch (input)
                {
                    case "1":
                        Console.Clear();
                        AddTask(taskList);
                        break;
                    case "2":
                        Console.Clear();
                        RemoveTask(taskList);
                        break;
                    case "3":
                        Console.Clear();
                        EditTask(taskList);
                        break;
                    case "4":
                        Console.Clear();
                        taskList.PrintAll(taskList.GetHead());
                        break;
                    case "5":
                        Console.Clear();
                        ViewTaskHistory(taskList);
                        break;
                    case "6":
                        SerializeToJson(taskList, jsonFilePath);
                        exit = true;
                        break;
                    case "7":
                        exit = true;
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Invalid option. Please try again.");
                        break;
                }

                Console.WriteLine();
            }

            // ======================================================
            /* var task1 = new Task("Task 1", "Description for Task 1", Priority.Medium, DateTime.Today.AddDays(3)); */
            /* task1.PushToHistory(); */
            /* var task2 = new Task("Task 2", "Description for Task 2", Priority.High, DateTime.Today.AddDays(1)); */
            /* task2.PushToHistory(); */
            /* var task3 = new Task("Task 3", "Description for Task 3", Priority.Low, DateTime.Today.AddDays(5)); */
            /* task3.PushToHistory(); */
            /* task3.SetName("Model"); */
            /* task3.PushToHistory(); */
            /* task3.SetName("Remove this one!"); */
            /* task3.PushToHistory(); */
            /* task3.PopFromHistory(); */
            /* task3.UndoLastChange(); */
            /**/
            /* taskList.Add(task1); */
            /* taskList.Add(task2); */
            /* taskList.Add(task3); */
            /**/
            /* SerializeToJson(taskList, jsonFilePath); */
            /**/
            /* LinkedList<Task> deserializedTaskList = DeserializeFromJson(jsonFilePath); */
            /* Console.WriteLine("Tasks from JSON:"); */
            /* deserializedTaskList.PrintAll(deserializedTaskList.GetHead()); */
            /**/
            /* Console.ReadLine(); */
        }
    }
}
